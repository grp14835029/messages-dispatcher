from django.apps import AppConfig


class MessagesDispatcherApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'messages_dispatcher_api'

    def ready(self):
        import messages_dispatcher_api.signals
