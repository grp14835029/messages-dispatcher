import json
from datetime import datetime, timezone, timedelta
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from messages_dispatcher_api.serializers import MsgListSerializer
from messages_dispatcher_api.tasks import msg_list_task
from messages_dispatcher_api.models import MsgList
from django_celery_beat.models import ClockedSchedule, PeriodicTask


@receiver(post_save, sender=MsgList)
def msg_list_created(sender, instance, created, **kwargs):
    if not created:
        return
    now = datetime.now(timezone.utc)
    if now > instance.date_end:
        return
    if instance.date_start - now > settings.get('SCHEDULE_BREAKPOINT_TIMEDELTA', timedelta(minutes=5)):
        # Задача будет запланирована в celery beat, чтобы задачи со стартом в дальнем будущем не висели в памяти worker
        schedule, created = ClockedSchedule.objects.get_or_create(clocked_time=instance.date_start)
        task_name = f"list_{instance.id}_scheduled_task"
        PeriodicTask.objects.create(
            clocked=schedule,
            one_off=True,
            name=task_name,
            task='messages_dispatcher_api.tasks.msg_list_task',
            args=json.dumps((MsgListSerializer(instance).data, task_name))
        )
        return
    # Т.к. задача должна быть выполнена или уже или в ближайшее время, то направляем эту задачу в celery worker
    eta = None if now > instance.date_start else instance.date_start
    msg_list_task.apply_async((MsgListSerializer(instance).data,), eta=eta)
