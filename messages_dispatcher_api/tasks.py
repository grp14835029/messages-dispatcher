from datetime import datetime, timezone
import random
from asgiref.sync import sync_to_async
from celery import shared_task
from django.db.models import Q
from django_celery_beat.models import PeriodicTask
from rest_framework.response import Response
from messages_dispatcher_api.models import MsgList, Client, Message, MessageStatus
import aiohttp
import asyncio
from django.conf import settings
from messages_dispatcher_api.serializers import MessageSerializer, MsgListSerializer


async def send_msg(lst, client, session, n_retries=5, msg=None):
    async def send_request(url):
        try:
            async with session.post(url, json=data,
                                    headers={'Authorization': f'Bearer {settings.MESSAGE_API_TOKEN}'},

                                    timeout=aiohttp.ClientTimeout(total=60)) as resp:
                resp.raise_for_status()
                json_resp = await resp.json()
                if json_resp['code'] != 0:
                    raise Exception('failed')
                return True, json_resp.get('message', '')
        except aiohttp.ClientError:
            return False, ''

    if not msg:
        params = {
            'date_send': datetime.now(timezone.utc),
            'msg_list': lst,
            'client': client,
            'status': MessageStatus.SENDING.value_str}
        log_msg = await sync_to_async(Message.objects.create)(**params)
    else:
        log_msg = msg
        log_msg.status = MessageStatus.SENDING.value_str
        await sync_to_async(log_msg.save)()
    if client.can_receive_messages(lst.allowed_intervals):
        data = {
            "id": log_msg.id,
            "phone": client.phone,
            "text": lst.message_text
        }
        res, msg = await send_request(settings.MESSAGE_API_BASE + f'send/{log_msg.id}')
    else:
        res, msg = False, ''
    if not res and n_retries > 0:
        schedule_send_msg_task.apply_async((MessageSerializer(log_msg).data, n_retries - 1),
                                           countdown=random.randint(60, 300))
        log_msg.status = MessageStatus.SCHEDULED.value_str
        await sync_to_async(log_msg.save)()
    else:
        log_msg.status = MessageStatus.SUCCESS.value_str if res else MessageStatus.FAIL.value_str
        log_msg.response_text = msg
        await sync_to_async(log_msg.save)()


async def send_all_messages(lst, clients):
    async with aiohttp.ClientSession() as session:
        tasks = [send_msg(lst, clt, session) async for clt in clients]
        await asyncio.gather(*tasks)


async def send_single_message(msg_id, n_retries):
    msg = await Message.objects.aget(pk=msg_id)
    clt = await sync_to_async(lambda: msg.client)()
    lst = await sync_to_async(lambda: msg.msg_list)()
    async with aiohttp.ClientSession() as session:
        await send_msg(lst, clt, session, n_retries, msg=msg)


@shared_task
def schedule_send_msg_task(var, n_retries):
    asyncio.run(send_single_message(var["id"], n_retries))


@shared_task
def msg_list_task(serialized_list, scheduled_task_name=None):
    if scheduled_task_name:  # Выполненную задачу можно удалить
        PeriodicTask.objects.get(name=scheduled_task_name).delete()

    lst = MsgList.objects.get(pk=serialized_list["id"])

    if datetime.now(timezone.utc) > lst.date_end:
        return

    q_expr = Q()
    if hasattr(lst.filter_props, 'get'):
        for filter_name in ('mob_code', 'tag'):
            if isinstance(lst.filter_props.get(filter_name), list) and \
                    len(lst.filter_props.get(filter_name)):
                q_expr = q_expr & Q(**{filter_name + '__in': lst.filter_props.get(filter_name)})
    clients = Client.objects.filter(q_expr)

    if not clients.count():
        return

    asyncio.run(send_all_messages(lst, clients))


def run_active():
    now = datetime.now(timezone.utc)
    lists = MsgList.objects.filter(date_start__lte=now, date_end__gte=now)
    for lst in lists:
        msg_list_task.apply_async((MsgListSerializer(lst).data,))
    ser = MsgListSerializer(lists, many=True)
    return Response(ser.data)
