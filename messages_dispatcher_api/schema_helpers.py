def get_map_param_to_actions(param, action_names=None):
    if action_names is None:
        action_names = ['create', 'retrieve', 'update', 'destroy', 'list', 'partial_update']
    return {key: param for key in action_names}
