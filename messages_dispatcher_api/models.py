from datetime import datetime, time, timezone
from enum import Enum
from django.db import models
from django.utils.translation import gettext_lazy as _


class MsgList(models.Model):
    date_start = models.DateTimeField()
    date_end = models.DateTimeField()
    message_text = models.CharField(max_length=200, blank=False)
    # property filters
    # {
    #   mob_code: ['123','234','356'],
    #   tag: ['tag1','vip','std']
    # }
    filter_props = models.JSONField(null=True)
    allowed_intervals = models.JSONField(null=True)

    def __str__(self):
        def truncate_string(val_str, max_len=10, suffix='...'):
            return val_str[:max_len] + suffix if len(val_str) > max_len else val_str

        return f'obj({self.pk}) "{truncate_string(self.message_text)}" {self.date_start}'


class Client(models.Model):
    phone = models.CharField(max_length=11, blank=False)
    mob_code = models.CharField(max_length=11, blank=False)
    tag = models.CharField(max_length=11, blank=True)
    time_zone = models.CharField(max_length=6, blank=False)

    def __str__(self):
        return f'obj({self.pk}) {self.phone}'

    def can_receive_messages(self, intervals, check_time=datetime.now(timezone.utc)):
        if intervals is None:
            return True
        client_tz = datetime.fromisoformat(check_time.replace(tzinfo=None).isoformat() + self.time_zone).tzinfo
        client_time = check_time.astimezone(client_tz).time().replace(tzinfo=None)
        for interval in intervals:
            tm_start = time.fromisoformat(interval[0]).replace(tzinfo=None)
            tm_end = time.fromisoformat(interval[1]).replace(tzinfo=None)
            if tm_start <= tm_end:
                if tm_start <= client_time <= tm_end:
                    return True
            else:
                if client_time >= tm_start or client_time <= tm_end:
                    return True
        return False


class MessageStatus(Enum):
    SENDING = ('sending', _('Message delivering to client'))
    FAIL = ('fail', _('Message delivery failed'))
    SUCCESS = ('success', _('Message successfully delivered'))
    SCHEDULED = ('scheduled', _('Message scheduled for retry sending'))

    def __init__(self, value_str, desc):
        self.value_str = value_str
        self.desc = desc


class Message(models.Model):
    date_send = models.DateTimeField()
    msg_list = models.ForeignKey(MsgList, on_delete=models.SET_NULL, null=True)
    client = models.ForeignKey(Client, on_delete=models.SET_NULL, null=True)
    status = models.CharField(max_length=10, choices=[(status.value_str, status.desc) for status in MessageStatus])
    response_text = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return f'obj({self.pk}) "{self.status}" {self.date_send}'
