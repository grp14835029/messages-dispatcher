from drf_spectacular.utils import extend_schema, inline_serializer, extend_schema_view
from rest_framework import viewsets, serializers
from rest_framework.decorators import action
from django.utils.translation import gettext_lazy as _
from messages_dispatcher_api.models import MsgList, Client, MessageStatus
from messages_dispatcher_api.reports import overall_report, detail_report
from messages_dispatcher_api.schema_helpers import get_map_param_to_actions
from messages_dispatcher_api.serializers import MsgListSerializer, ClientSerializer, MessageSerializer
from messages_dispatcher_api.tasks import run_active


@extend_schema_view(
    **get_map_param_to_actions(extend_schema(tags=['Message lists'])))
class MsgListViewSet(viewsets.ModelViewSet):
    queryset = MsgList.objects.all()
    serializer_class = MsgListSerializer

    @extend_schema(
        responses={
            200: inline_serializer(
                name='Overall_report',
                fields={'msg_list': serializers.PrimaryKeyRelatedField(allow_null=True,
                                                                       queryset=MsgList.objects.all(),
                                                                       required=False),
                        'date_start': serializers.DateTimeField(),
                        'date_end': serializers.DateTimeField(),
                        } |
                       {status.value_str: serializers.IntegerField(help_text=status.desc)
                        for status in MessageStatus},
                many=True
            )
        },
        tags=['Reports']
    )
    @action(detail=False, methods=['get'], name=_('Message lists overall statistic'), url_name='overall_report',
            url_path='stat')
    def overall_report_action(self, request, *args, **kwargs):
        return overall_report()

    @extend_schema(
        responses={200: MessageSerializer(many=True)},
        tags=['Reports']
    )
    @action(detail=True, methods=['get'], name=_('Message list detailed statistic'), url_name='detail_report',
            url_path='stat')
    def detail_report_action(self, request, *args, **kwargs):
        return detail_report(self.get_object())

    @extend_schema(
        responses={200: MsgListSerializer(many=True)},
        tags=['Message lists'],
    )
    @action(detail=False, methods=['get'], name=_('Run active lists'), url_name='run_active',
            url_path='run')
    def run_active_action(self, request, *args, **kwargs):
        return run_active()


@extend_schema_view(
    **get_map_param_to_actions(extend_schema(tags=['Clients'])))
class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
