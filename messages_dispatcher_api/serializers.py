from datetime import datetime, time
from rest_framework import serializers
from jsonschema import validate
from messages_dispatcher_api.models import MsgList, Client, Message
from django.utils.translation import gettext_lazy as _


class MsgListSerializer(serializers.ModelSerializer):
    class Meta:
        model = MsgList
        fields = ['id', 'date_start', 'date_end', 'message_text', 'filter_props', 'allowed_intervals']

    @staticmethod
    def validate_allowed_intervals(value):
        if value is None:
            return value
        schema = {
            "type": "array",
            "minItems": 1,
            "items": {
                "minItems": 2,
                "maxItems": 2,
                "type": "array",
                "items": {
                    "type": "string"
                }
            }
        }
        try:
            validate(instance=value, schema=schema)
            list(map(lambda i: list(map(lambda tm: time.fromisoformat(tm), i)), value))
        except Exception as e:
            raise serializers.ValidationError(repr(e))

        return value

    def validate(self, data):
        dt_start = data.get('date_start') or getattr(self.instance, 'date_start')
        dt_end = data.get('date_end') or getattr(self.instance, 'date_end')
        if dt_start > dt_end:
            raise serializers.ValidationError(_(f"date_start can not be later than date_end"))

        return data


class ClientSerializer(serializers.ModelSerializer):
    phone = serializers.RegexField(r'^7\d{10}$', required=False)

    class Meta:
        model = Client
        fields = ['id', 'phone', 'mob_code', 'tag', 'time_zone']

    @staticmethod
    def validate_time_zone(value):
        now = datetime.now()
        try:
            datetime.fromisoformat(now.isoformat() + value)
        except ValueError:
            raise serializers.ValidationError(_(f"Wrong time_zone format"))
        return value


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ['id', 'date_send', 'msg_list', 'client', 'status', 'response_text']
