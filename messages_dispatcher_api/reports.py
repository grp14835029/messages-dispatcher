from django_pandas.io import read_frame
from rest_framework.response import Response
from messages_dispatcher_api.models import MsgList, Message
from messages_dispatcher_api.serializers import MessageSerializer


def overall_report():
    lst = MsgList.objects.all()
    msg = Message.objects.all()
    df_lst = read_frame(lst, fieldnames=['id', 'date_start', 'date_end'], verbose=False).rename(
        columns={'id': 'msg_list'})
    df_msg = read_frame(msg, fieldnames=['msg_list', 'status'], verbose=False)

    df_msg['msg_list'].fillna(-1, inplace=True)
    df_msg = df_msg.groupby('msg_list')['status'].value_counts()
    df_msg = df_msg.to_frame().rename(columns={'status': 'count'})
    df_msg.reset_index(inplace=True)
    df_msg = df_msg.pivot_table(index='msg_list', columns='status', values='count', fill_value=0)
    df_msg.reset_index(inplace=True)

    df = df_lst.merge(df_msg, how='outer')
    df.fillna(0, inplace=True)

    return Response(df.to_dict(orient='records'))


def detail_report(list_instance):
    ser = MessageSerializer(list_instance.message_set.all(), many=True)
    return Response(ser.data)
